using System.Diagnostics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Nancy.Owin;
using Serilog;
using Serilog.Core;
using Serilog.Formatting.Compact;

namespace ConnectBoard
{
    public class Startup
    {
        private readonly Logger _logger;

        public Startup(IHostingEnvironment env)
        {
            var processId = Process.GetCurrentProcess().Id;

            _logger = new LoggerConfiguration()
                .Enrich.WithMachineName()
                .WriteTo.Console(new RenderedCompactJsonFormatter())
                .WriteTo.RollingFile(new RenderedCompactJsonFormatter(), "logs/ConnectBoard-{Date}-" + processId + ".json", shared: true)
                .CreateLogger();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseOwin(x => x.UseNancy(opt =>
            {
                opt.Bootstrapper = new NancyCoreBootstrapper(Program.AppConfig, _logger);
            }));
        }
    }
}