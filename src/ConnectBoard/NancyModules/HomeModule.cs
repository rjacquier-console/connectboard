using System.Threading;
using System.Threading.Tasks;
using Nancy;
using Serilog;
using StackExchange.Redis;

namespace ConnectBoard.NancyModules
{
    public sealed class HomeModule : NancyModule
    {
        private readonly ILogger _logger;
        private readonly IConnectionMultiplexer _redis;

        public HomeModule(ILogger logger, IConnectionMultiplexer redis)
        {
            _logger = logger;
            _redis = redis;

            Get("/",
                action: Home,
                condition: null,
                name: nameof(Home));
        }

        private async Task<object> Home(dynamic o, CancellationToken cancellationToken)
        {
            var db = _redis.GetDatabase();

            var result = await db.StringIncrementAsync("ConnectBoardVisits");

            _logger.Information("Received request for root path: ConnectBoardVisits:{ConnectBoardVisits}", result);
            return $"Hello from Kestrel, OWIN and Nancy running on CoreCLR, you are visitor {result}";
        }
    }
}