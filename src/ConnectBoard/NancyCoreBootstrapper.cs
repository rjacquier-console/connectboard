using System;
using System.Net;
using Microsoft.Extensions.Configuration;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using Serilog;
using StackExchange.Redis;

namespace ConnectBoard
{
    public class NancyCoreBootstrapper : DefaultNancyBootstrapper
    {
        private readonly IConfiguration _appConfig;
        private readonly ILogger _logger;

        public NancyCoreBootstrapper(IConfiguration appConfig, ILogger logger)
        {
            _appConfig = appConfig;
            _logger = logger;
        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            var requestLog = _logger.ForContext("InstanceId", Guid.NewGuid());
            container.Register(requestLog);

            _logger.Information("AppConfig[redis]: " + _appConfig["redis"]);

            var dnsTask = Dns.GetHostAddressesAsync(_appConfig["redis"]);
            var addresses = dnsTask.Result;
            var redis = ConnectionMultiplexer.Connect(addresses[0].MapToIPv4().ToString());
            container.Register<IConnectionMultiplexer>(redis);
        }

        protected override void RequestStartup(TinyIoCContainer requestContainer, IPipelines pipelines, NancyContext context)
        {
            var logger = requestContainer.Resolve<ILogger>().ForContext("RequestId", Guid.NewGuid());
            requestContainer.Register(logger);
        }
    }
}