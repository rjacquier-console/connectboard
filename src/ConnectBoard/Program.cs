﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Reflection;
using System;

namespace ConnectBoard
{
    public class Program
    {
        internal static IConfigurationRoot AppConfig;

        public static void Main(string[] args)
        {

            AppConfig = new ConfigurationBuilder()
                .SetBasePath(AssemblyDirectory)
                .AddJsonFile(Path.Combine(AssemblyDirectory, "appconfig.json"), false)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();

            Console.WriteLine("Appsetting[redis]: " + AppConfig["redis"]);

            var host = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseKestrel()
                .UseConfiguration(AppConfig)
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetEntryAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }
}