#csproj file


# Build solution, create container
./build.sh

# Create and run new container (daemon)
docker run -p 8020:8020 -d connectboard

# Create and run new container
docker run -p 8020:8020 connectboard

# Docker containers running (-a all)
docker ps

# logs
docker logs [ContainerId|ContainerName]

# Run another process in a running container (interactive)
docker exec -ti [name] bash

# Image history
docker history [ImageId|Name(:tag)]
docker history microsoft/dotnet
docker history microsoft/dotnet:latest

# Image details
docker image inspect [ImageId|Name(:tag)]

# Create a new image from a container
docker commit [ContainerId|ContainerName] connectboard:1.0.0-beta

# For ES requirement
sudo sysctl -w vm.max_map_count=262144

# Kibana creds
elasticsearch.username: elastic
elasticsearch.password: changeme