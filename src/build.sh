#!/usr/bin/env bash
dotnet restore --runtime debian.8-x64
dotnet publish --framework netcoreapp1.1 --runtime debian.8-x64 --configuration Release -o out
docker build -t connectboard .