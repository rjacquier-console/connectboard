Hack to get C Drive shared
Set-NetConnectionProfile -interfacealias "vEthernet (DockerNAT)" -NetworkCategory Private
Reset Shared C Drive in Docker settings

ConnectBoard:
http://localhost:8020/

Redis Commander:
http://localhost:8081/

Cerebro
http://localhost:9000/#/connect
http://elasticsearch:9200
elastic
changeme

Kibana
http://localhost:5601