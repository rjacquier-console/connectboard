# Node - Typescript - Gulp - Visual Studio Code (VSCode) - Example project

Using Node version v8.4.0 (via nvm)

## Getting

Install all required node modules:

`npm install`


## Visual Studio shortcut keys

* **Build**: ctrl-shift-B
* **Debug**: F5