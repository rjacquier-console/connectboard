# stop all containers:
docker kill $(docker ps -q)

# remove all containers
docker rm $(docker ps -a -q)

# remove all docker volumes
docker volume ls -qf dangling=true | xargs -r docker volume rm

# remove all docker images
#docker rmi $(docker images -q)

rm -rf ./data/*
rm -rf ./logs/*